import "reflect-metadata";
import app from "./app";
import {AppDataSource} from "./config/db"

async function main() {
  try {
    await AppDataSource.initialize();
    console.log("Database connected");
    app.listen(3000);
    console.log('Server is Listen port', 3000)
  } catch (error) {
    console.log(error);
  }
}

main();

